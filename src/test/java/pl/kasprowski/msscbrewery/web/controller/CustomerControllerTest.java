package pl.kasprowski.msscbrewery.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.kasprowski.msscbrewery.services.CustomerService;
import pl.kasprowski.msscbrewery.web.model.CustomerDto;

import java.util.UUID;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
class CustomerControllerTest {

    @Mock
    private CustomerService service;

    @InjectMocks
    private CustomerController controller;

    private MockMvc mockMvc;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller)
                .setControllerAdvice(new ValidationExceptionHandler())
                .build();
    }

    @Test
    void getCustomerById() throws Exception {
        final UUID id = UUID.randomUUID();
        final CustomerDto dto = CustomerDto.builder()
                .id(id)
                .name("someName")
                .build();

        when(service.getById(eq(id))).thenReturn(dto);

        mockMvc.perform(get("/api/v1/customer/" + id)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", equalTo(id.toString())))
                .andExpect(jsonPath("$.name", equalTo("someName")));
    }

    @Test
    void saveNewCustomer() throws Exception {
        final ObjectMapper mapper = new ObjectMapper();
        final UUID id = UUID.randomUUID();
        final CustomerDto dto = CustomerDto.builder()
                .name("someName")
                .build();

        final CustomerDto created = CustomerDto.builder()
                .id(id)
                .name("someName")
                .build();

        when(service.save(eq(dto))).thenReturn(created);

        mockMvc.perform(post("/api/v1/customer")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(dto)))
                .andExpect(status().isCreated())
                .andExpect(header().string(HttpHeaders.LOCATION, "/api/v1/customer/" + id));
    }

    @Test
    void UpdateCustomer() throws Exception {
        final ObjectMapper mapper = new ObjectMapper();
        final UUID id = UUID.randomUUID();
        final CustomerDto dto = CustomerDto.builder()
                .id(id)
                .name("someName")
                .build();

        mockMvc.perform(put("/api/v1/customer/" + id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(dto)))
                .andExpect(status().isNoContent());

        verify(service, times(1)).update(eq(id), eq(dto));
    }

    @Test
    void deleteCustomer() throws Exception {
        final UUID id = UUID.randomUUID();

        mockMvc.perform(delete("/api/v1/customer/" + id))
                .andExpect(status().isNoContent());

        verify(service, times(1)).delete(eq(id));
    }
}
