package pl.kasprowski.msscbrewery.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import pl.kasprowski.msscbrewery.services.BeerService;
import pl.kasprowski.msscbrewery.web.model.BeerDto;

import java.util.UUID;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
class BeerControllerTest {

    private MockMvc mockMvc;

    @Mock
    private BeerService service;

    @InjectMocks
    private BeerController controller;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(controller)
                .setControllerAdvice(new ValidationExceptionHandler())
                .build();
    }

    @Test
    void getBeer() throws Exception {
        final UUID id = UUID.randomUUID();
        final BeerDto dto = BeerDto.builder()
                .id(id)
                .beerName("sample")
                .build();

        when(service.getById(eq(id))).thenReturn(dto);

        mockMvc.perform(get("/api/v1/beer/" + id)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", equalTo(id.toString())))
                .andExpect(jsonPath("$.beerName", equalTo("sample")));
    }

    @Test
    void saveBeer() throws Exception {
        final ObjectMapper mapper = new ObjectMapper();
        final BeerDto dto = BeerDto.builder()
                .beerName("sample")
                .beerStyle("somestyle")
                .upc(1L)
                .build();

        final UUID id = UUID.randomUUID();
        final BeerDto savedDto = BeerDto.builder()
                .id(id)
                .beerName("sample")
                .build();

        when(service.save(eq(dto))).thenReturn(savedDto);

        mockMvc.perform(post("/api/v1/beer")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(dto)))
                .andExpect(status().isCreated())
                .andExpect(header().string(HttpHeaders.LOCATION, "/api/v1/beer/" + id.toString()));
    }

    @Test
    void updateBeer() throws Exception {
        final ObjectMapper mapper = new ObjectMapper();

        final UUID id = UUID.randomUUID();
        final BeerDto dto = BeerDto.builder()
                .beerName("sample")
                .beerStyle("somestyle")
                .upc(1L)
                .build();

        mockMvc.perform(put("/api/v1/beer/" + id.toString())
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(dto)))
                .andExpect(status().isNoContent());

        verify(service, times(1)).update(eq(id), eq(dto));
    }

    @Test
    void deleteBeer() throws Exception {
        final UUID id = UUID.randomUUID();

        mockMvc.perform(delete("/api/v1/beer/" + id))
                .andExpect(status().isNoContent());

        verify(service, times(1)).delete(eq(id));
    }
}