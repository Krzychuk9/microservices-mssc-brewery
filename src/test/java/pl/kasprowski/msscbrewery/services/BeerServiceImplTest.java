package pl.kasprowski.msscbrewery.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.kasprowski.msscbrewery.web.model.BeerDto;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

class BeerServiceImplTest {

    private BeerService service;

    @BeforeEach
    void setUp() {
        service = new BeerServiceImpl();
    }

    @Test
    void shouldGetById() {
        final UUID id = UUID.randomUUID();

        final BeerDto beer = service.getById(id);

        assertThat(beer).isNotNull();
        assertThat(beer).extracting(BeerDto::getId).isEqualTo(id);
    }

    @Test
    void shouldCreateNewBeer() {
        final BeerDto dto = BeerDto.builder()
                .beerName("name")
                .beerStyle("style")
                .upc(1L)
                .build();

        final BeerDto saveBeer = service.save(dto);

        assertThat(saveBeer).isNotNull();
        assertThat(saveBeer).extracting(BeerDto::getId).isNotNull();
        assertThat(saveBeer).extracting(BeerDto::getBeerName).isEqualTo("name");
        assertThat(saveBeer).extracting(BeerDto::getBeerStyle).isEqualTo("style");
        assertThat(saveBeer).extracting(BeerDto::getUpc).isEqualTo(1L);
    }
}
