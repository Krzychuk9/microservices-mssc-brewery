package pl.kasprowski.msscbrewery.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.kasprowski.msscbrewery.web.model.CustomerDto;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

class CustomerServiceImplTest {

    private CustomerService service;

    @BeforeEach
    void setUp() {
        service = new CustomerServiceImpl();
    }

    @Test
    void shouldGetById() {
        final UUID id = UUID.randomUUID();

        final CustomerDto customer = service.getById(id);

        assertThat(customer).isNotNull();
        assertThat(customer).extracting(CustomerDto::getId).isEqualTo(id);
        assertThat(customer).extracting(CustomerDto::getName).isEqualTo("hard-coded");
    }

    @Test
    void shouldCreateNewCustomer() {
        final CustomerDto dto = CustomerDto.builder()
                .name("name")
                .build();

        final CustomerDto savedDto = service.save(dto);

        assertThat(savedDto).isNotNull();
        assertThat(savedDto).extracting(CustomerDto::getId).isNotNull();
        assertThat(savedDto).extracting(CustomerDto::getName).isEqualTo("name");
    }
}
