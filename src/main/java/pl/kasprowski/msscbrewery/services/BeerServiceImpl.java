package pl.kasprowski.msscbrewery.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.kasprowski.msscbrewery.web.model.BeerDto;

import java.util.UUID;

@Slf4j
@Service
public class BeerServiceImpl implements BeerService {

    @Override
    public BeerDto getById(final UUID id) {
        return BeerDto.builder()
                .id(id)
                .beerName("Galaxy Cat")
                .beerStyle("Pale Ale")
                .upc(1L)
                .build();
    }

    @Override
    public BeerDto save(final BeerDto beerDto) {
        return BeerDto.builder()
                .id(UUID.randomUUID())
                .beerName(beerDto.getBeerName())
                .beerStyle(beerDto.getBeerStyle())
                .upc(beerDto.getUpc())
                .build();
    }

    @Override
    public void update(final UUID id, final BeerDto beerDto) {
        //TODO
    }

    @Override
    public void delete(final UUID beerId) {
        log.debug("Deleting beer {}", beerId);
    }
}
