package pl.kasprowski.msscbrewery.services;

import pl.kasprowski.msscbrewery.web.model.BeerDto;

import java.util.UUID;

public interface BeerService {
    BeerDto getById(UUID id);

    BeerDto save(BeerDto beerDto);

    void update(UUID id, BeerDto beerDto);

    void delete(UUID beerId);
}
