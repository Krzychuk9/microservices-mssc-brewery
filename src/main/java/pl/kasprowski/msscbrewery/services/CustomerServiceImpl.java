package pl.kasprowski.msscbrewery.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.kasprowski.msscbrewery.web.model.CustomerDto;

import java.util.UUID;

@Slf4j
@Service
public class CustomerServiceImpl implements CustomerService {

    @Override
    public CustomerDto getById(final UUID id) {
        return CustomerDto.builder()
                .id(id)
                .name("hard-coded")
                .build();
    }

    @Override
    public CustomerDto save(final CustomerDto dto) {
        return CustomerDto.builder()
                .id(UUID.randomUUID())
                .name(dto.getName())
                .build();
    }

    @Override
    public void update(final UUID id, final CustomerDto dto) {
        log.debug("Updating {} {}", id, dto.getName());
    }

    @Override
    public void delete(final UUID id) {
        log.debug("Deleting {}", id);
    }
}
