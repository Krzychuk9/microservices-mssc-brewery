package pl.kasprowski.msscbrewery.services;

import pl.kasprowski.msscbrewery.web.model.CustomerDto;

import java.util.UUID;

public interface CustomerService {
    CustomerDto getById(UUID id);

    CustomerDto save(CustomerDto dto);

    void update(UUID id, CustomerDto dto);

    void delete(UUID id);
}
